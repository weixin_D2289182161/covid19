package com.study.datasource.crawler;


import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.Timer;
import java.util.TimerTask;

/**
 * 演示jdk自带的定时任务API
 * 无法处理异常，假如某一任务执行失败，也会影响后续任务执行
 */
//@Component//表示将该类交给spring管理，作为Spring中的对象
public class SchedulerTest {
    public static void main(String[] args) {
        Timer timer = new Timer();
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                System.out.println("每个1秒执行一次");
            }
        },1000,1000);
    }
    //springboot的定时任务工具
    //@Scheduled(initialDelay = 1000,fixedDelay = 1000)
    //@Scheduled(cron = "0/1 * * * * ?")//每隔一秒执行一次 0 0/1 * * * ?（一分钟）
    //@Scheduled(cron = "0 0 8 * * ?")//每天8点定时执行
    public void scheduler(){
        System.out.println("每个一秒-cron执行一次");
    }
}
