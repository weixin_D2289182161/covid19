package com.study.datasource.bean;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 用来封装各省市的疫情数据的javabean
 */
// 由于idea版本过低不再支持此插件 8.8
@Data
@NoArgsConstructor
@AllArgsConstructor
public class CovidBean {
    	   private String provinceName;// 省份名称
           private String provinceShortName; // 省份短名
           private String cityName;
           private Integer currentConfirmedCount; // 当前确诊人数
           private Integer confirmedCount; // 累计确诊人数
           private Integer suspectedCount; // 疑似确诊人数
           private Integer curedCount; // 治愈人数
           private Integer deadCount; // 死亡人数
           private Integer locationId; // 位置id
           private Integer pid; // 位置id
           private String statisticsData; // 每一天的统计数据
           private String cities; //下属城市
           private String datetime; //时间



}
