package com.study.datasource.bean;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 模拟防疫物资数据的javaBean
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class MateriaBean {
    private String name;
    private String from;
    private Integer count;
}
