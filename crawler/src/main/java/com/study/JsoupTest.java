package com.study;

import org.apache.commons.io.FileUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.net.URL;

/**
 * 演示使用Jsoup实现页面解析
 */
public class JsoupTest {
    @Test
    public void testGetDoument() throws Exception {
//        final Document doc = Jsoup.connect("https://www.itcast.cn/").get();
//        final Document doc = Jsoup.parse(new URL("https://www.itcast.cn/"), 1000);
//        final Document doc = Jsoup.parse(new File("jsoup.html"), "UTF-8");
        final String htmlStr = FileUtils.readFileToString(new File("jsoup.html"), "UTF-8");
        final Document doc = Jsoup.parse(htmlStr);
        System.out.println(doc);
        final Element titleElement = doc.getElementsByTag("title").first();
        final String title = titleElement.text();
        System.out.println(title);//传智播客官网-一样的教育,不一样的品质
    }
    @Test
    public void testGetElement() throws Exception {
        final Document doc = Jsoup.parse(new File("jsoup.html"), "UTF-8");

        // 根据id获取元素
        final Element element = doc.getElementById("city_bj");
        final String text = element.text();
        System.out.println(text);//北京中心

        // 根据标签获取元素
        final Elements elements = doc.getElementsByTag("title");
        final Element titleElement = elements.first();
        final String title = titleElement.text();
        System.out.println(title);//传智播客官网-一样的教育,不一样的品质

        // 根据class获取元素
        final Element element1 = doc.getElementsByClass("s_name").last();
        final String text1 = element1.text();
        System.out.println(text1);//广州

        // 根据属性获取元素
        final String abc = doc.getElementsByAttribute("abc").first().text();
        System.out.println(abc);//广州
    }

    @Test
    public void testElementOperator() throws Exception {
        final Document doc = Jsoup.parse(new File("jsoup.html"), "UTF-8");
        final Element element = doc.getElementsByAttributeValue("class", "city_con").first();
        // 获取元素中id
        final String id = element.id();
        System.out.println(id);//city_id
        // 获取元素中的classname
        final String className = element.className();
        System.out.println(className);//city_con
        // 获取元素中的属性值
        final String id1 = element.attr("id");
        System.out.println(id1);//city_id
        // 从元素中获取所有的属性
        final String attrs = element.attributes().toString();
        System.out.println(attrs);// id="city_id" class="city_con" style="display: none;"
        // 获取元素的文本内容
        final String text = element.text();
        System.out.println(text);//北京 上海 广州 天津
    }


    @Test
    public void testSelect() throws Exception {
        final Document doc = Jsoup.parse(new File("jsoup.html"), "UTF-8");
        // 根据标签名获取元素
        final Elements spans = doc.select("span");
        System.out.println(spans.text());//北京 上海 广州
        // 根据id获取元素
        final String text = doc.select("#city_bj").text();
        System.out.println(text);//北京中心
        // 根据class获取元素
        final String text1 = doc.select(".class_a").text();
        System.out.println(text1);//北京
        // 根据属性获取元素
        final String text2 = doc.select("[abc]").text();
        System.out.println(text2);//广州
        // 根据属性值获取元素
        final String text3 = doc.select("[class=s_name]").text();
        System.out.println(text3);//北京 上海 广州
    }
    @Test
    public void  testSelect2() throws Exception {
        final Document doc = Jsoup.parse(new File("jsoup.html"), "UTF-8");
        // 根据元素+id组合选取元素
        final String text = doc.select("li#test").text();
        System.out.println(text);//北京
        // 根据标签名+class
        final String text1 = doc.select("li.class_a").text();
        System.out.println(text1);//北京
        // 根据标签名+元素名
        final String text2 = doc.select("span[abc]").text();
        System.out.println(text2);//广州
        // 任意组合
        final String text3 = doc.select("span[abc].s_name").text();
        System.out.println(text3);//广州
        // 查找某个元素下的直接子元素
        final String text4 = doc.select(".city_con > ul > li ").text();
        System.out.println(text4);//北京 上海 广州
        // 查找某个元素下的所有子元素
        final String text5 = doc.select(".city_con li").text();
        System.out.println(text5);//北京 上海 广州 天津
        // 查找某个元素下的所有直接子元素
        final String text6 = doc.select(".city_con > *").text();
        System.out.println(text6);//北京 上海 广州 天津

    }
}
