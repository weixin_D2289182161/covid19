package com.study;

import org.apache.http.HttpHost;
import org.apache.http.NameValuePair;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.junit.Test;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

/**
 * 演示使用HttpClientTest实现网络爬虫
 */
public class HttpClientTest {
    @Test
    public void testGet() throws IOException {
        // 1.创建HttpClient对象
        final CloseableHttpClient httpClient = HttpClients.createDefault();
        // 2.创建HttpGet请求并配置
        final HttpGet httpGet = new HttpGet("https://www.itcast.cn/?username=java");
        httpGet.setHeader("user-agent","Mozilla/5.0 (Linux; Android 6.0; Nexus 5 Build/MRA58N) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/103.0.0.0 Mobile Safari/537.36");
        // 3.发起请求
        final CloseableHttpResponse response = httpClient.execute(httpGet);
        // 4.判断响应状态码获取响应的数据
        if(response.getStatusLine().getStatusCode()==200){//200表示响应成功
            final String html = EntityUtils.toString(response.getEntity(), "UTF-8");
            System.out.println(html);
        }
        // 5.关闭资源
        httpClient.close();
        response.close();
    }
    @Test
    public void testPost() throws Exception {
        // 1.HttpClient对象
        final CloseableHttpClient httpClient = HttpClients.createDefault();
        // 2.创建httppost对象并进行设置
        final HttpPost httpPost = new HttpPost("https://www.itcast.cn/");
        // 以集合来存放请求参数
        List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("username","java"));
        final UrlEncodedFormEntity entity = new UrlEncodedFormEntity(params, "UTF-8");
        httpPost.setEntity(entity);// 设置请求体/参数
        httpPost.setHeader("user-agent","Mozilla/5.0 (Linux; Android 6.0; Nexus 5 Build/MRA58N) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/103.0.0.0 Mobile Safari/537.36");
        // 3.发起请求
        final CloseableHttpResponse response = httpClient.execute(httpPost);
        // 4.响应状态码并获取资源
        if (response.getStatusLine().getStatusCode()==200){
            final String html = EntityUtils.toString(response.getEntity(), "UTF-8");
            System.out.println(html);
        }
        // 5.关闭数据
        response.close();
        httpClient.close();
    }

    @Test
    public void testPool() throws Exception {
        // 1.创建HttpClient连接管理器
        final PoolingHttpClientConnectionManager cm = new PoolingHttpClientConnectionManager();
        // 2.设置参数
        cm.setMaxTotal(200);//设置最大连接数
        cm.setDefaultMaxPerRoute(20);//设置每个主机的最大并发
        doGet(cm);
        doGet(cm);
    }

    private void doGet(PoolingHttpClientConnectionManager cm) throws Exception {
        // 3.从连接池中获取HttpClient对象
        final CloseableHttpClient httpclient = HttpClients.custom().setConnectionManager(cm).build();
        // 此处的断电调试可以看出每次从连接池中获取到的对象不一样
        // 4.创建HttpGet对象
        final HttpGet httpGet = new HttpGet("https://www.itcast.cn/");
        // 5.发起请求
        final CloseableHttpResponse response = httpclient.execute(httpGet);
        // 6.获取数据
        if (response.getStatusLine().getStatusCode()==200){
            final String html = EntityUtils.toString(response.getEntity(), "UTF-8");
            System.out.println(html);
        }
        // 7.关闭资源
        response.close();
        // httpClient.close();不能关闭HttpClient对象，因为使用了连接池，HttpClient对象使用完后要放回到池中，而不是关闭
    }

    @Test
    public void testConfig() throws Exception {
        // 0.创建请求配置对象
        final RequestConfig requestConfig = RequestConfig.custom()
                .setSocketTimeout(10000)//设置连接超时时间
                .setConnectTimeout(10000)//设置创建连接超时时间
                .setConnectionRequestTimeout(10000)// 设置请求超时时间
                .setProxy(new HttpHost("",0000))// 添加代理服务器
                .build();
        // 1.创建HttpClient对象
//        final CloseableHttpClient httpClient = HttpClients.createDefault();
        final CloseableHttpClient httpClient = HttpClients.custom().setDefaultRequestConfig(requestConfig).build();
        // 2.创建HttpGet对象
        final HttpGet httpGet = new HttpGet("https://www.itcast.cn/");
        // 3.发生请求
        final CloseableHttpResponse response = httpClient.execute(httpGet);
        // 4.获取数据
        if (response.getStatusLine().getStatusCode()==200){
            final String html = EntityUtils.toString(response.getEntity(), "UTF-8");
            System.out.println(html);
        }
        // 5.关闭资源
        response.close();
        httpClient.close();
    }

}
