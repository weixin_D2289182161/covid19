package com.study;

import org.junit.Test;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

/**
 * 演示使用JDK自带的API实现网路爬虫
 */
public class JDKAPITest {
    @Test
    public void testGet() throws Exception {
        // 1.获取要访问的URL
        URL url = new URL("https://www.itcast.cn/?username=xx");
        // 2.获取连接对象
        HttpURLConnection urlConnection = (HttpURLConnection)url.openConnection();
        // 3.配置连接信息：请求方式，请求参数，请求头...
        urlConnection.setRequestMethod("GET");// 请求方式默认为GET,注意要大写
        urlConnection.setRequestProperty("user-agent","Mozilla/5.0 (Linux; Android 6.0; Nexus 5 Build/MRA58N) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/103.0.0.0 Mobile Safari/537.36");
        urlConnection.setConnectTimeout(30000);//设置超时时间30秒
        // 4.获取数据
        InputStream in = urlConnection.getInputStream();
        BufferedReader reader = new BufferedReader(new InputStreamReader(in));
        String line;
        String html ="";
        while ((line=reader.readLine())!=null){
            html+=line + "\n";
        }
        System.out.println(html);
        // 5.关闭资源
        in.close();
        reader.close();
    }
    @Test
    public void testPost() throws Exception {
        URL url = new URL("https://www.itcast.cn/");

        HttpURLConnection urlConnection = (HttpURLConnection)url.openConnection();

        urlConnection.setDoOutput(true);//允许向URL输出内容
        urlConnection.setRequestMethod("POST");// 请求方式默认为GET,注意要大写
        urlConnection.setRequestProperty("user-agent",": Mozilla/5.0 (Linux; Android 6.0; Nexus 5 Build/MRA58N) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/103.0.0.0 Mobile Safari/537.36");
        urlConnection.setConnectTimeout(30000);//设置超时时间30秒
        final OutputStream out = urlConnection.getOutputStream();
        out.write("username=xxx".getBytes());

        InputStream in = urlConnection.getInputStream();
        BufferedReader reader = new BufferedReader(new InputStreamReader(in));
        String line;
        String html ="";
        while ((line=reader.readLine())!=null){
            html+=line + "\n";
        }
        System.out.println(html);

        in.close();
        reader.close();

    }


}
